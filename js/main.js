/*
	speaks phrase from text with whatever contours
		single ReaderSpeaker
		feed it text
	while speaking, starts listening:
		create, activate, & store a ListenerSpeaker
		onsoundend:
			- check for ListenerSpeaker in storage that isn't self
				- if exists, it starts listening
				- else, make a new one, store it, start listening


	ReaderSpeaker (x1)
	[ListenerSpeaker, (+ListenerSpeaker, +)]
*/

var dried = {
	SCRAMBLE: false,
	PHRASE_CHANCE: 0.5,
	LANG: "a",
	PITCH_LO: 0, // LS uses these values
	PITCH_HI: 9,
	RATE_LO: 0.1,
	RATE_HI: 3,
	WAIT_LO: 25,
	WAIT_HI: 300,
	_PITCH_LO: 0, // dried.varyPerformance uses these to change the values LS sees
	_PITCH_HI: 9,
	_PITCH_MIN_RANGE: 2, // minimum breadth of range (eg, 0-2 or 2-4)
	_PITCH_MAX_RANGE: 9, // maximum breadth of range (eg, 0-9)
	_RATE_LO: 0.1,
	_RATE_HI: 3,
	_RATE_MIN_RANGE: 0.2,
	_RATE_MAX_RANGE: 1,
	_WAIT_LO: 25,
	_WAIT_HI: 500,
	_WAIT_MIN_RANGE: 25,
	_WAIT_MAX_RANGE: 200,
	_VP_INT_MIN: 5000, // ms
	_VP_INT_MAX: 20000,
	COLLECTION_INTERVAL: 5000, // ms between automated speech collections. 2sec seems to be minimum viable to catch something evaluatable.
	VARY_PERFORMANCE_INTERVAL: 0,
	TIME_SINCE_LAST_VARY: 0,
	// VARY_PERFORMANCE_TIMER: undefined,
	MAX_SILENCE: 4000,
	SILENCE_TIMER: undefined,
	RAND: function(lo, hi) {
			return Math.random() * (hi - lo) + lo;
		},
	collectSpeech: function() {
		console.log("stopping activeListener");
		if (!!dried.activeListener && !!dried.activeListener.stop)
		    dried.activeListener.stop();
	},
	active: false, // acts as override for shutting off individual voices. should only be true when everything is running.
	voices: [new ListenerSpeaker()],
	activeListener: undefined, // filled by ListenerSpeakers when they start listening, cleared when they stop.
	// the following is from ECMA 262 6.0 section 6.1 rev 15 Jan 2015, p17-18 https://www.ecma-international.org/ecma-262/6.0/#sec-ecmascript-language-types
	sourceText: "The Undefined type has exactly one value, called undefined. Any variable that has not been assigned a value has the value undefined. The Null type has exactly one value, called null. The Boolean type represents a logical entity having two values, called true and false. The String type is the set of all finite ordered sequences of zero or more 16-bit unsigned integer values (“elements”). The String type is generally used to represent textual data in a running ECMAScript program, in which case each element in the String is treated as a UTF-16 code unit value. Each element is regarded as occupying a position within the sequence. These positions are indexed with nonnegative integers. The first element (if any) is at index 0, the next element (if any) at index 1, and so on. The length of a String is the number of elements (i.e., 16-bit values) within it. The empty String has length zero and therefore contains no elements. Where ECMAScript operations interpret String values, each element is interpreted as a single UTF-16 code unit. However, ECMAScript does not place any restrictions or requirements on the sequence of code units in a String value, so they may be ill-formed when interpreted as UTF-16 code unit sequences. Operations that do not interpret String contents treat them as sequences of undifferentiated 16-bit unsigned integers. No operations ensure that Strings are in a normalized form. Only operations that are explicitly specified to be language or locale sensitive produce language-sensitive results NOTE The rationale behind this design was to keep the implementation of Strings as simple and high-performing as possible. If ECMAScript source code is in Normalized Form C, string literals are guaranteed to also be normalized, as long as they do not contain any Unicode escape sequences. Some operations interpret String contents as UTF-16 encoded Unicode code points. In that case the interpretation is: A code unit in the range 0 to 0xD7FF or in the range 0xE000 to 0xFFFF is interpreted as a code point with the same value. A sequence of two code units, where the first code unit c1 is in the range 0xD800 to 0xDBFF and the second code unit c2 is in the range 0xDC00 to 0xDFFF, is a surrogate pair and is interpreted as a code point with the value (c1 - 0xD800) × 0x400 + (c2 – 0xDC00) + 0x10000. A code unit that is in the range 0xD800 to 0xDFFF, but is not part of a surrogate pair, is interpreted as a code point with the same value. The Symbol type is the set of all non-String values that may be used as the key of an Object property (6.1.7). Each possible Symbol value is unique and immutable. Each Symbol value immutably holds an associated value called [[Description]] that is either undefined or a String value.",
	displayUtterance: function(utterance) {
		var p = document.createElement('p');
		p.textContent = utterance.text;
		dried.utterances.insertBefore(p, dried.utterances.firstElementChild);
	},
	varyPerformance: function() {
		speechSynthesis.cancel();
		dried.TIME_SINCE_LAST_VARY = new Date().getTime();

		var thisPitchRange = dried.RAND(dried._PITCH_MIN_RANGE, dried._PITCH_MAX_RANGE);
		dried.PITCH_LO = dried.RAND(0, dried._PITCH_HI - thisPitchRange);
		dried.PITCH_HI = dried.PITCH_LO + thisPitchRange;

		var thisRateRange = dried.RAND(dried._RATE_MIN_RANGE, dried._RATE_MAX_RANGE);
		dried.RATE_LO = dried.RAND(0, dried._RATE_HI - thisRateRange);
		dried.RATE_HI = dried.RATE_LO + thisRateRange;

		var thisWaitRange = dried.RAND(dried._WAIT_MIN_RANGE, dried._WAIT_MAX_RANGE);
		dried.WAIT_LO = dried.RAND(0, dried._WAIT_HI - thisWaitRange);
		dried.WAIT_HI = dried.WAIT_LO + thisWaitRange;

		dried.SCRAMBLE = Math.random() < 0.5;
		dried.VARY_PERFORMANCE_INTERVAL = parseInt(dried.RAND(dried._VP_INT_MIN, dried._VP_INT_MAX));
		// dried.VARY_PERFORMANCE_TIMER = window.setTimeout(function(){ dried.varyPerformance(); }, dried.VARY_PERFORMANCE_INTERVAL);

		console.log("varied. pitch: " + dried.PITCH_LO + " - " + dried.PITCH_HI + ", rate: " + dried.RATE_LO + " - " + dried.RATE_HI + ", wait: " + dried.WAIT_LO + " - " + dried.WAIT_HI + ", scramble: " + dried.SCRAMBLE + ". next after " + dried.VARY_PERFORMANCE_INTERVAL + " ms.");
		

		dried.VARY_COLOUR();
	},
	DO_SILENCE_TIMER: function() {
		if (dried.SILENCE_TIMER) {
			window.clearTimeout(dried.SILENCE_TIMER); // resets timer, called when silence interrupted
		}
		dried.SILENCE_TIMER = window.setTimeout(function() { // restarts performance if there's too long of a silence
			// console.log('silent');
			dried.clickStop();
			dried.clickStart();
		}, dried.MAX_SILENCE);
	},
	// DO_END_TIMER: function() {
	// 	var duration = dried.RAND(2 * 60000, 10 * 60000);
	// 	console.log('ending in ' + (duration / 1000) + ' sec');
	// 	dried.END_TIMER = window.setTimeout(function(){
	// 		console.log('finito');
	// 		dried.clickStop();
	// 	}, duration);
	// },
	SET_CAN_END: function() {
		var duration = parseInt(dried.RAND(60000, 2 * 60000));
		console.log('can end in ' + (duration / 1000) + ' sec');
		dried.CAN_END_IS_SET = true;
		window.setTimeout(function(){
			dried.CAN_END = true;
		}, duration);
	},
	END_CHANCE: 0.3,
	CHECK_IF_END: function() {
		if (dried.RAND(0,1) < dried.END_CHANCE && dried.CAN_END) {
			console.log('complete.');
			dried.DO_END();
			return true;
		}
	},
	DO_END: function() {
		dried.clickStop();

		window.setTimeout(function(){ // wait a bit, then turn screen white
			document.body.style.backgroundColor = '#EEE';
			dried.BEEP();
			window.setTimeout(function() { // and then tell the piece to end
				document.body.className = "complete";
				document.body.style.backgroundColor = '';
				dried.CAN_END = dried.CAN_END_IS_SET = false;
			}, 2000);
		}, 2000);
		
		return;
	},
	VARY_COLOUR: function() {
		var rgbvals = parseInt(dried.RAND(50, 150)) + ", " 
			+ parseInt(dried.RAND(50, 150)) + ", " 
			+ parseInt(dried.RAND(50, 150));
		document.body.style.backgroundColor = 'rgb(' + rgbvals + ')';
	},
	BEEP: function() {
		var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
		snd.play();
	}
};


(function() {

	/**
	  *
	  * ONLOAD SCRIPT
	  *
	 **/
	dried.header = document.getElementsByTagName('h1')[0];
	dried.listeningStatus = document.getElementById('listening-status');
	dried.speakingStatus = document.getElementById('speaking-status');
	dried.utterances = document.getElementById('utterances');
	speechSynthesis.cancel();
	dried.setListeningStatus = function (statusOn) {
		if (statusOn) {
			dried.listeningStatus.textContent = "listening";
		} else {
			dried.listeningStatus.textContent = "not listening";
		}
	}
	dried.setSpeakingStatus = function (statusOn) {
		if (statusOn) {
			dried.speakingStatus.textContent = "speaking";
		} else {
			dried.speakingStatus.textContent = "not speaking";
		}
	}

	// set ending function
	// dried.voices[0].onend = voiceEnded();


	// speak & start listening on click
	dried.clickStart = function () {
		if (dried.CHECK_IF_END()) {
			return;
		}

		if (!dried.CAN_END_IS_SET) {
			dried.SET_CAN_END();
		}

		dried.active = true;
		//clear and reset end timer
		// window.clearTimeout(dried.END_TIMER);
		// dried.DO_END_TIMER();

		beginSpeaking(); // shit hits the FAN!
		dried.voices[0].toggleRecog();
		dried.interval = window.setInterval(dried.collectSpeech, dried.COLLECTION_INTERVAL);
		// dried.header.textContent = 'Click to stop';
		document.body.className = "active";

		window.removeEventListener("click", dried.clickStart);
		window.addEventListener("click", dried.clickStop);
	}
	dried.clickStop = function() {
		dried.active = false;
		window.clearInterval(dried.interval);
		window.clearTimeout(dried.VARY_PERFORMANCE_TIMER);
		window.clearTimeout(dried.SILENCE_TIMER);
		// window.clearTimeout(dried.END_TIMER);
		for (var i in dried.voices) {
			dried.voices[i].stop();
		}
		speechSynthesis.cancel();
		// dried.header.textContent = 'Click to start';
		document.body.className = "rest_position";
		document.body.style.backgroundColor = '';

		window.removeEventListener("click", dried.clickStop);
		window.addEventListener("click", dried.clickStart);
	}
	window.addEventListener("click", dried.clickStart);

	// stops the active listener, triggering a new one (theoretically)
	window.addEventListener('keydown', function(e){
		if (e.keyCode == 32 || e.keyCode == 13) {
			dried.collectSpeech();
		}
	})

	function beginSpeaking() {
		// if(!dried.VARY_PERFORMANCE_TIMER)
		if (new Date().getTime() - dried.TIME_SINCE_LAST_VARY > dried.VARY_PERFORMANCE_INTERVAL)
			dried.varyPerformance(); // start these guys
		dried.DO_SILENCE_TIMER(); // start silence timers

		// generate some kernel text
		var textArray = dried.sourceText.split(' ');
		var firstLength = parseInt(dried.RAND(2, 20));
		var text = '';
		for (var i = 0; i < firstLength; i++) {
			text += textArray[parseInt(Math.random() * textArray.length)] + ' ';
		}

		var text = dried.voices[0].scrambleWords(text); // depends on function in ListenerSpeaker
		text = dried.voices[0].createMessages(text);

		// set msgs to play the next one
		var y = 0;
		for (var i = 0; i < text.length; i++) {
			text[i].onend = function() {
				setTimeout(function() {
					y++;
					dried.displayUtterance(text[y]);
					speechSynthesis.speak(text[y]);
				}, dried.RAND(dried.WAIT_LO, dried.WAIT_HI));
			}
		}
		// turn recognition on after final message
		text[text.length - 1].onend = function(e) {
			dried.setSpeakingStatus(false);
		};

		// start chain
		dried.setSpeakingStatus(true);
		dried.displayUtterance(text[0]);
		speechSynthesis.speak(text[0]);
	}

	function startFullscreen() {
		if (document.documentElement.webkitRequestFullscreen)
			document.documentElement.webkitRequestFullscreen();
		else
		    document.documentElement.requestFullscreen();
	}

	function stopFullscreen() {
		if (document.documentElement.webkitExitFullscreen)
			document.documentElement.webkitExitFullscreen();
		else
		    document.documentElement.exitFullscreen();
	}

	function isFullscreen() {
		if ('webkitFullscreenElement' in document)
			return !!document.webkitFullscreenElement;
		else if ('fullscreenElement' in document)
			return !!document.fullscreenElement;
	}

	function toggleFullscreen() {
		if (!document.fullscreenElement) {
		    startFullscreen();
		} else {
		    if (document.exitFullscreen) {
			stopFullscreen();
		    }
		}
	}

	document.addEventListener('keydown', function (e) {
		if (e.keyCode == 13)
			toggleFullscreen();
	}, false);
})();
