// define Listener pseudo-class

function ListenerSpeaker() {
	var outLS = new webkitSpeechRecognition();

	outLS.continuous = false;
	outLS.isListening = false;
	outLS.lang = "en-US";

	outLS.onresult = function(e) {
		outLS.toggleRecog(true);

		var result = outLS.getFinalResult(e.results);
		if (result == false) {
			return;
		}

		dried.previousEvent = e;
		result = result[0].transcript;

		if (dried.SCRAMBLE)
			result = outLS.scrambleWords(result);

		var msgs = outLS.createMessages(result); // []

		// set msgs to play the next one
		var y = 0;
		for (var i in msgs) {
			msgs[i].onend = function() {
				setTimeout(function() {
					y++;
					dried.displayUtterance(msgs[y]);
					speechSynthesis.speak(msgs[y]);
				}, outLS.waitAfter);
			}
		}

		// turn recognition on after final message
		msgs[msgs.length - 1].onend = function(e) {
			y = 0;
			dried.setSpeakingStatus(false);
			dried.DO_SILENCE_TIMER(); // reset timer at end of speaking
		};

		// start chain
		dried.setSpeakingStatus(true);
		dried.displayUtterance(msgs[0]);
		dried.DO_SILENCE_TIMER(); // also reset at beginning of speaking so it doesn't go off in the middle
		speechSynthesis.speak(msgs[0]);
	}

	outLS.onstart = function() {
		outLS.isListening = true;
		dried.setListeningStatus(true);
		dried.activeListener = this;
	}

	// fired when speech ends. onaudioend fires here and at onend.
	// outLS.onspeechend = function() {
	// 	console.log('onspeechend');
	// }

	// fired before results in, and also at speech collection
	outLS.onaudioend = function() {
		// console.log('onaudioend');
		dried.setListeningStatus(false);
		dried.activeListener = undefined;

		// override.
		if (!dried.active) {
			// console.log('killsig');
			outLS.isListening = false;
			return;
		}

		var voiceNum = false;

		// check if available voice
		for (var i = 0; i < dried.voices.length; i++) {
			if (!dried.voices[i].isListening) {
				// console.log('voice ' + i + ' is not listening; let\'s start it');
				voiceNum = i;
				break;
				break;
			} else {
				// console.log('voice ' + i + ' is listening');
			}
		}

		outLS.isListening = false;

		// if not, make one at end of array
		if (voiceNum === false) {
			voiceNum = dried.voices.length;
			// console.log('making new voice at ' + i);
			dried.voices.push(new ListenerSpeaker());
		}

		// then tell it to start listening
		dried.voices[voiceNum].toggleRecog(false); // listen
	}

	outLS.onnomatch = function(){
		// console.log('no match, using previous');
		this.onresult(dried.previousEvent);
	}

	// gets final result or returns false if there is none.
	// may behave differently now that continuous is off.
	outLS.getFinalResult = function (results) {
		for (var i = 0; i < results.length; i++) {
				if (results[i].isFinal) {
					return results[i];
				}
			}

		return false;
	}

	// scrambles word order of a string
	outLS.scrambleWords = function(text) {
		var words = text.split(' ');

		text = '';
		do {
			text += words.splice(parseInt(Math.random() * words.length), 1) + " ";
		} while (words.length > 0);

		// pop trailing space
		text.trimRight();

		return text;
	}


	// creates an array of SpeechSynthesisUtterance objects from a string
	// using groupings of random lengths of that string in order
	outLS.createMessages = function(text) {
		text.trim();
		var out = [];
		var words = text.split(' ');
		var phrases = [];
		var phraseChance = dried.PHRASE_CHANCE; // likelihood of being a phrase.
		var i;

		do {
			// determine if "phrase", and pushes appropriately
			if (words[0].trim().length == 0) {
				words.shift();
			} else if (Math.random() < phraseChance) {
				i = parseInt(Math.random() * words.length + 1); // phrase of random length

				// make phrase
				var thisString = '';
				for (var j = 0; j < i; j++) {
					thisString += words.shift() + " ";
				}
				thisString.trim();

				// add to phrases
				if (thisString.length > 0)
					phrases.push(thisString);
			} else {
				// otherwise, add single word
				phrases.push(words.shift());
			}
		} while (words.length > 0);

		// console.log('messages:');
		// console.log(phrases);

		// creates SpeechSynthesisUtterance objects
		do {
			var msg = new SpeechSynthesisUtterance(phrases.shift());
			msg.lang = dried.LANG;
			msg.pitch = dried.RAND(dried.PITCH_LO, dried.PITCH_HI);
			msg.rate = dried.RAND(dried.RATE_LO, dried.RATE_HI);
			msg.waitAfter = dried.RAND(dried.WAIT_LO, dried.WAIT_HI);
			out.push(msg)
		} while (phrases.length > 0);

		return out;
	}

	// toggle recognition on / off, optionally force stop
	outLS.toggleRecog = function(forceStop) {
		if (forceStop || outLS.isListening && !forceStop) {
			outLS.stop();
			return;
		} else if (!outLS.isListening) {
			try {
				outLS.start();
			} catch (e) { }
			return;
		}
	}

	return outLS;
}

// ListenerSpeaker.prototype = webkitSpeechRecognition.prototype;
// ListenerSpeaker.prototype.constructor = ListenerSpeaker;
